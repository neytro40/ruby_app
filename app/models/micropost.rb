class Micropost < ApplicationRecord
    validates :content, length: { maximum: 340 }
    belongs_to :user
end
