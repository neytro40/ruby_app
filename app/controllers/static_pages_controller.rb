class StaticPagesController < ApplicationController
  def home
  end

  def around
  end

  def about
  end

  def other
  end
  
  def articles
  end
end
