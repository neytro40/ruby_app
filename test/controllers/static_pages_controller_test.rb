require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
    def setup
      @base_title = "Herocu research"
    end
  
  test "should get home" do
    get static_pages_home_url
    assert_response :success
  end

  test "should get around" do
    get static_pages_around_url
    assert_response :success
  end

  test "should get about" do
    get static_pages_about_url
    assert_response :success
  end

  test "should get other" do
    get static_pages_other_url
    assert_response :success
  end

end
