Rails.application.routes.draw do
  resources :microposts
  resources :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'static_pages#home'
  get 'home' => 'static_pages#home'
  get 'about' => 'static_pages#about'
  get 'around' => 'static_pages#around'
  get 'other' => 'static_pages#other'
  get 'articles' => 'articles#articles'
end
